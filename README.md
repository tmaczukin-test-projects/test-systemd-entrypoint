# Test systemd entrypoint

Tested with such configuration of the GitLab Runner:

```toml
concurrent = 1
check_interval = 0

[[runners]]
  name = "apollo-systemd-ready"
  url = "https://gitlab.com/"
  token = "--REDACTED--"
  executor = "docker-ssh"
  builds_dir = "/root/builds"
  [runners.ssh]
    port = "22"
    user = "root"
    password = "root"
  [runners.docker]
    volumes = ["/cache", "/sys/fs/cgroup:/sys/fs/cgroup:ro"]
    privileged = true
    cap_add = ["SYS_ADMIN"]
    security_opt = ["seccomp:unconfined"]
```

