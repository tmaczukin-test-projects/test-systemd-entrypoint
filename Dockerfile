FROM centos:7.0.1406

ENV container docker

RUN yum -y swap -- remove fakesystemd -- install systemd systemd-libs; \
    yum clean all
RUN yum -y update; \
    yum clean all

RUN yum install https://repo.saltstack.com/yum/redhat/salt-repo-2017.7-1.el7.noarch.rpm -y; \
    yum clean all
RUN yum install salt-minion -y --nogpgcheck; \
    yum clean all

RUN yum -y install hostname git openssh-server; \
    yum clean all; \
    ln -sf /usr/lib/systemd/system/sshd.service /etc/systemd/system/multi-user.target.wants/sshd.service; \
    echo "root:root" | chpasswd

RUN mkdir /root/builds

WORKDIR /root
EXPOSE 22

ENTRYPOINT ["/usr/sbin/init"]
